# SSS Market

![lib](https://static.wixstatic.com/media/71ed93_58892dddd49d427bad982f2324c095d0~mv2.png)

Introduction 
-------------------------
> Sharing Clothes Platform
> Nền tảng thời trang hướng tới việc chia sẻ và làm mới tủ đồ. Mua nhiều và bán nhanh, tạo ra trải nghiệm tốt nhất cho tất cả các tín đồ thời trang.

Requirement to build
-------------------------
  - [Flutter v2.2.3](https://docs.flutter.dev/development/tools/sdk/releases/) - Flutter is an open source framework by Google for building beautiful, natively compiled, multi-platform applications from a single codebase.
  - IDE - Integrated Development Environment
    1. [Visual Studio Code](https://code.visualstudio.com/)
    2. Android Studio| prefer v4.1.2
      [Stable](https://developer.android.com/studio/) 
      [Version 4.1.2](https://androidstudio.googleblog.com/2021/01/android-studio-412-available.html/)
  - [Git](https://git-scm.com/download) - Git is a free and open source distributed version control system desgined to handle everything from small to very large projects with speed and efficiency.


Usage 
-------------------------
From your command line:

* Clone this repository
  * `git clone https://gitlab.com/papagroup-outsource/sharing-clothes-platform.git`

* Go into the repository
  * `cd sharing-closthes-platform`

* Install dependencies
  * `flutter pub get`

* Run the app
  * `flutter run`

Notice: If you don't compile with error like "Flutter Compile Error on Firebase Auth: "A non-null value must be returned since the return type 'Never' doesn't allow null.", try to remove circumflex of library in [pubspec.yaml](https://gitlab.com/papagroup-outsource/sharing-clothes-platform/-/blob/develop/pubspec.yaml/).

Ex: firebase_messaging: ^10.0.4 ==> firebase_messaging: 10.0.4

Deploy dSYMs file to firebase: replace 2021-05-20++
-------------------------
ios/Pods/FirebaseCrashlytics/upload-symbols -gsp ios/Runner/GoogleService-Info.plist -p ios ~/Library/Developer/Xcode/Archives/2021-05-20/Runner\ 5-20-21,\ 8.02\ PM.xcarchive/dSYMs

or: sh deploy-sdyms.sh ~/Library/Developer/Xcode/Archives/2021-05-20/Runner\ 5-20-21,\ 8.02\ PM.xcarchive/dSYMs
